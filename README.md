# URL Title Fetching Proxy Microservice

Use the power of (my fork of) [GoOse][goose] to fetch URL metadata for things like [iOS Workflow][wf] to process.

## Rationale

I was originally using a [Pythonista script](https://gist.github.com/rjp/294b1184eade4f898ba1) to fetch titles and clean URLs
for posting into [Buffer](https://buffer.com/) but ran into a variety of problems (SSL verification, lack of Pythonista updates).

Then I tried [Workflow][wf] with limited success.

Finally I took some code I'd written for a bulletin board bot using [GoOse][goose] and created a small microservice
using `net/http` to take in an encoded URL, fetch the content, and return a JSON blob with the metadata.

## Example

The URL input is Base64 encoded with a simple prefix to mitigate DOS attacks.

    > echo -n ZZQQhttps://soundcloud.com/zimpenfish/dark-ambient-victoria-line|base64|tr -d '\n'
    WlpRUWh0dHBzOi8vc291bmRjbG91ZC5jb20vemltcGVuZmlzaC9kYXJrLWFtYmllbnQtdmljdG9yaWEtbGluZQ==
    > curl http://host:port/bouncer/WlpRUWh0dHBzOi8vc291bmRjbG91ZC5jb20vemltcGVuZmlzaC9kYXJrLWFtYmllbnQtdmljdG9yaWEtbGluZQ==
    {"URL":"https://soundcloud.com/zimpenfish/dark-ambient-victoria-line","Title":"Dark Ambient Victoria Line","Description":"Created from the samples kindly provided by TFL and Matt Rogers - http://art.tfl.gov.uk/projects/sample-set/","Canonical":"https://soundcloud.com/zimpenfish/dark-ambient-victoria-line"}

[goose]: https://github.com/rjp/GoOse
[wf]: https://workflow.is/


package main

import (
	"encoding/base64"
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/rjp/GoOse"
)

var logger *log.Logger
var debugger *log.Logger

type URLtoPost struct {
	URL         string
	Title       string
	Description string
	Canonical   string
}

func proxyHandler(w http.ResponseWriter, r *http.Request) {
	debugger.Println(r)

	// Strip the leading `/` from the path
	b64url := r.URL.Path[len("/bouncer/"):]

	requestedURL, err := base64.StdEncoding.DecodeString(b64url)
	if err != nil {
		panic(err)
	}

	token := requestedURL[:3]
	realURL := requestedURL[3:]

	logger.Println(string(token), string(realURL))

	g := goose.New("config.json")
	article := g.ExtractFromUrl(string(realURL))

	if !strings.Contains(article.ContentType, "text/html") {
		debugger.Println("Probably not HTML")
		return
	}

	if article.TopNode == nil {
		debugger.Println("NILNODE ==> ", requestedURL)
	}

	url_response := URLtoPost{}

	url_response.URL = article.FinalUrl
	url_response.Title = strings.Replace(article.Title, "\n", " ", -1)
	url_response.Canonical = article.CanonicalLink
	url_response.Description = article.MetaDescription

	debugger.Println(url_response)

	j, err := json.Marshal(url_response)
	if err != nil {
		panic(err)
	}

	w.Write(j)
}

func main() {
	logger = log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lmicroseconds|log.Lshortfile)
	debugger = log.New(os.Stderr, "DEBUG:", log.Ldate|log.Ltime|log.Lmicroseconds|log.Lshortfile)

	bindhost := flag.String("bind", "localhost", "Host/IP to bind to")
	bindport := flag.String("port", "9988", "Port to bind to")
	debugging := flag.Bool("debugging", false, "Turn on debugging output")
	flag.Parse()

	if !*debugging {
		debugger.SetOutput(ioutil.Discard)
	}

	http.HandleFunc("/bouncer/", proxyHandler)
	http.ListenAndServe(*bindhost+":"+*bindport, nil)
}
